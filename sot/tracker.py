import types
import numpy as np
import time

class Tracker():
  def __init__(self, first_objects, multi_tracking=True, id=0, metric="cosine", cuda=False):
    global np
    if cuda:
      import cupy
      np = cupy
      del cupy
    else:
      import numpy
      np = numpy
      del numpy

    self._cuda = cuda

    self._metric = self._cosine_similarity
    if type(metric) == str:
      if metric == "cosine":
        self._metric = self._cosine_similarity
      elif metric == "euclidean":
        self._metric = self._euclidean
    elif type(metric) == types.FunctionType:
      self._metric = metric

    self._old_objects = [*first_objects]
    self._multi_tracking = multi_tracking
    self._id = id
    self._count = 0

  def reset(self, first_objects):
    self.__init__(first_objects, self._multi_tracking, self._id, self._metric, self._cuda)

  def _cosine_similarity(self, old_object, now_object):
    old_object = old_object.reshape((-1,))
    now_object = now_object.reshape((-1,))
    if np.sum(old_object) == 0.0 or np.sum(now_object) == 0.0:
      similarity = 0
    else:
      similarity = np.dot(old_object, now_object) / (np.linalg.norm(old_object) * np.linalg.norm(now_object))
    return similarity

  def _euclidean(self, old_object, now_object):
    old_object = old_object.reshape((-1,))
    now_object = now_object.reshape((-1,))
    if np.sum(old_object) == 0.0 or np.sum(now_object) == 0.0:
      euclidian = 0
    else:
      euclidian = 1 / (1 + np.linalg.norm(old_object - now_object))
    return euclidian

  def metrics(self, old_frame, new_frame):
    objects = []
    for n, new in enumerate(new_frame):
      objects.append([])
      for o, old in enumerate(old_frame):
        objects[n].append(self._metric(old, new))
    return np.array(objects, dtype="float32")

  def find_matching(self, metrics, solved=None):
    cs = metrics.copy()
    if solved is None:
      solved = (cs[:,0] * 0).astype("bool")
      self._track = ((cs[:,0] * 0) + -1).astype("int8")
      self._scores = (cs[:,0] * 0).astype("float32")

    if solved.astype("uint8").sum() is len(solved):
      track = self._track.copy()
      del self._track 
      scores = self._scores.copy()
      del self._scores 
      return track, scores

    i = 0
    a = cs[i,:].argmax()
    av = cs[i,a]
    while(av == 0.0 and solved[i]):
      i += 1
      if i == len(cs):
        track = self._track.copy()
        del self._track 
        scores = self._scores.copy()
        del self._scores 
        return track, scores
      a = cs[i,:].argmax()
      av = cs[i, a]

    touched_x = []
    touched_y = []

    while True:
      if len(touched_x) >= len(cs[0]) and len(touched_y) >= len(cs[:,0]):
        break
        
      b = cs[:,a].argmax()
      bv = cs[b,a]

      if (bv == 0.0 or bv == av) and not solved[b] and av != 0.0:
        solved[b] = True
        self._track[b] = -1 if bv == 0.0 else a
        self._scores[b] = av
        cs[b,:] = 0.0
        cs[:,a] = 0.0
        return self.find_matching(cs, solved)

      touched_y.append(b)
      c = cs[b,:].argmax()
      cv = cs[b, c]
      
      if (cv == 0.0 or cv == bv) and not solved[b] and bv != 0.0:
        solved[b] = True
        self._track[b] = -1 if cv == 0.0 else c
        self._scores[b] = cv
        cs[b,:] = 0.0
        cs[:,c] = 0.0
        return self.find_matching(cs, solved)
      
      a, av = c, cv
      touched_x.append(c)

    track = self._track.copy()
    del self._track
    scores = self._scores.copy()
    del self._scores 
    return track, scores

  def predict(self, now_objects, verbose=True):
    old = self._old_objects
    now = [*now_objects]

    all = time.time()
    start = all
    metrics = self.metrics(old, now)
    metrics_end = time.time()
    metrics_time = metrics_end - start
    track, scores = self.find_matching(metrics)
    find_track_end = time.time()
    find_track_time = find_track_end - metrics_end
    all_time = find_track_end - all

    ret = []

    for n, s, obj in zip(track, scores, now):
      if n == -1:
        if self._multi_tracking:
          ret.append((len(self._old_objects), 0.0))
        else:
          ret.append((-1, s))
        self._old_objects.append(obj)
      else:
        if self._multi_tracking:
          ret.append((int(n), s))
        else:
          if n == self._id:
            ret.append((self._id, s))
          else:
            ret.append((-1, s))
        self._old_objects[int(n)] = obj

    self._count += 1

    if verbose:
      print("Frame", self._count, ":")
      print("Old objects     =", len(self._old_objects))
      print("Now objects     =", len(now))
      print("Metrics time    = %.03f s" % metrics_time)
      print("Find track time = %.03f s" % find_track_time)
      print("Total time      = %.03f s" % all_time)
      print("Results         =", ("|" + ("%10d|" * len(track))) % (*track,))
      print("Scores          =", ("|" + ("%.08f|" * len(track))) % (*scores,))
      print()
      
    return ret

  def pred(self, now_objects):
    return self.predict(now_objects, False)

  find = pred

  __call__ = find