from sot.tracker import Tracker
import numpy as np
import matplotlib.pyplot as plt

metric = lambda x, y: np.sqrt((y ** 2) + (x ** 2))
# metric = lambda x, y: np.abs(1 - np.abs(y - x))

max_objects = 7

test = np.random.sample((50,max_objects,))
f = [*test]

tracker = Tracker(f[0][:1], metric=metric)

a = [tracker.predict(i[:np.random.randint(1, max_objects - 1)]) for i in f[1:]]

x = {}
y = {}
m = 0
for n, i in enumerate(a):
    for o, s in i:
        if s != 0.0:
            y[o] = y.get(o, [])
            y[o].append(s)
            x[o] = x.get(o, [])
            x[o].append(n)
            m = s if s > m else m

fig = plt.figure()
for n, (a, b) in enumerate(list(zip(x.values(), y.values()))):
    plt.plot(a[:1] + a + a[-1:], [0] + b + [0], "o-", label="%d" % n)

plt.legend(loc="upper left")
plt.ylim(0.0, m + 1.0)
plt.show()
