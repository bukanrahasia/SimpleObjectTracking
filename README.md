# Simple Object Tracking

This project is single/multi object tracking only.

## Installation

### (Default) CPU processing
    
```bash
pip install numpy
```    

### CUDA processing

```bash
pip install cupy
```